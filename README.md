# cGPSmapper map format to CSV
## Introduction
The [polish format](https://wiki.openstreetmap.org/wiki/Polish_format) is used by cGPSmapper to create vector maps for Garmin GPS.

This ruby script extract points of interest (POI) from polish format .mp files and print them in comma separated values format (CSV).

## Usage

./mp2csv.rb [_FILE_]

The csv listing is displayed on standard output. On unix systems, you can redirect the output to a file this way :

./mp2csv.rb _input.mp_ > _output_file_

In polish format, POI are described by fields : type, value, note, etc... The list of these fields is defined in line 2 of the script. The program will display an alert for unknown fields, giving the line number where they occurs. You should do a first run to check for these unknown fields, and add them in the second line of the script, or correct them in the mp file if they are mispelled.
