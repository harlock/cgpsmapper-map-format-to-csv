#!/usr/bin/env ruby
champs=['type','label','levels','light','foundationcolor','depth','lighttype','period','note']

if ARGV.length < 1
	puts 'Nom de fichier manquant'
	exit
end

if File.exist?(ARGV[0])

	puts champs.join(",")+",latitude,longitude"
	listechamps=Hash.new
	champs.each {|key| listechamps[key]=''}
	
	f=File.open(ARGV[0], 'r')
	f.each_line do |line|
	    if (line.chomp=='[RGN10]' || line.chomp=='[RGN20]' || line.chomp == '[POI]')
	    	coord=Array.new
	    	valeurs=listechamps.clone
	    	while((line=f.gets) && ! line.include?('END'))
	    		champ=String.new
	    		valeur=String.new
	    		champ,valeur = line.chomp.split(/\s*=\s*/)
	    		
	    		if (champ[0]==';' || champ.downcase =~ /origin[1-2]/)
	    			next
	    		elsif (champ.downcase=='origin0')
	    			coord.push(valeur.delete('()'))
	    		else
					if (valeurs[champ.downcase])
						valeurs[champ.downcase]=valeur
					else
						puts '>>> '+f.lineno.to_s+' : Champ inconnu : '+champ.to_s
					end
	    		end
	    		
	    	end
	    	if (valeurs['type'].include? 'x')
	    		valeurs['type']=valeurs['type'].hex.to_s
	    	end
	    	desc=valeurs.values.join(",")+','
	    	coord.each {|xy| puts desc+xy}
	    end
	end
else
	puts 'Ce fichier n\'existe pas'
end
